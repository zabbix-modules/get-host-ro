How to install : 

- Go to your zabbix frontend file installation (default : /usr/share/zabbix/modules)
- Type git clone with the url of repo.
- Change the owner of directory to your web user (ex www-data) with chown -R www-data. get-host-ro
- Go to your web zabbix interface Menu -> Administration -> Gereral -> Modules
- Use the Scan directory button on the top
- Enable the module
- Go to Menu Monitoring to use it
- Enjoy !


![Screenshot](https://framagit.org/zabbix-modules/get-host-ro/-/raw/main/screenshot.png)